//
//  ContentView.swift
//  Refreshing
//
//  Created by William Ong on 3/24/23.
//

import SwiftUI
import UserNotifications

let userDefaults = UserDefaults.standard
var goalSave = userDefaults.integer(forKey: KeyValue.goal.rawValue)
var totalWaterSave = userDefaults.double(forKey: KeyValue.totalWater.rawValue)
var unitsSave = userDefaults.string(forKey: KeyValue.units.rawValue)
var notiHrSave = userDefaults.integer(forKey: KeyValue.notiHr.rawValue)
var notiMinSave = userDefaults.integer(forKey: KeyValue.notiMin.rawValue)
var darkModeSave = userDefaults.bool(forKey: KeyValue.darkMode.rawValue)
var notiOnSave = userDefaults.bool(forKey: KeyValue.notiOn.rawValue)
var percentSave = userDefaults.double(forKey: KeyValue.percent.rawValue)
let waterFont = Font.custom("Futura", size: 46)

class GlobalVars: ObservableObject {
    @Published var totalWater: Double = totalWaterSave == 0.0 ? 0.0 : totalWaterSave
    @Published var changeWater = 0
    @Published var goal: Int = goalSave == 0 ? 64 : goalSave
    @Published var units: String = unitsSave ?? "oz."
    @Published var notiHr: Int = notiHrSave == 0 && notiMinSave == 0 ? 1 : notiHrSave
    @Published var prevNotiHr = 1
    @Published var notiMin: Int = notiMinSave == 0 ? 0 : notiMinSave
    @Published var prevNotiMin = 0
    @Published var darkMode: Bool = darkModeSave
    @Published var notiOn: Bool = notiOnSave
    @Published var percent: Double = percentSave == 0 ? 0.00001 : percentSave
}

enum KeyValue: String {
    case totalWater, goal, units, notiHr, notiMin, darkMode, notiOn, percent
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

struct ContentView: View {
    @Environment(\.colorScheme) var colorScheme
    @StateObject var globals = GlobalVars()
    var body: some View {
        TabView {
            HomeView()
                .tabItem() {
                    Image(systemName: "house.fill").resizable()
                        .frame(width: 40, height: 40, alignment: .leading)
                    Text("Home")
                }
            SettingsView()
                .tabItem() {
                    Image(systemName: "gearshape.fill").resizable()
                        .frame(width: 40, height: 40, alignment: .trailing)
                    Text("Settings")
                }
        }
        .environmentObject(globals)
        .onAppear {
            if (!globals.darkMode && colorScheme == .dark) {
                globals.darkMode = true
                userDefaults.set(true, forKey: KeyValue.darkMode.rawValue)
            }
        }
    }
}

struct HomeView: View {
    @EnvironmentObject var globals: GlobalVars
    @State private var waveOffset = Angle(degrees: 0)
    @State var showAddView = false
    @State var showRemoveView = false
    var body: some View {
        ZStack {
            VStack (spacing: 0) {
                if (globals.units == "oz.") {
                    Text("\(Int(globals.totalWater)) \(globals.units)")
                        .font(waterFont)
                } else {
                    Text("\(globals.totalWater, specifier: "%.1f") \(globals.units)")
                        .font(waterFont)
                }
                ZStack {
                    Image("straw")
                        .resizable()
                        .offset(x: 23, y: -38)
                        .frame(width: 8, height: 460)
                        .rotationEffect(.degrees(27.3))
                        .shadow(radius: 2)
                    ZStack(alignment: .center) {
                        Rectangle()
                            .fill(globals.darkMode ? Color.white.opacity(0.15): Color.gray.opacity(0.08))
                            .frame(width: 400, height: 400)
                        
                        Wave(offset: Angle(degrees: self.waveOffset.degrees), percent: globals.percent)
                            .fill(Color.cyan)
                            .frame(width: 400, height: 440)
                    }
                    .mask {
                        Image("cup")
                            .resizable()
                            .frame(width: 320, height: 420)
                            .shadow(color: .gray, radius: 5)
                    }
                }
                HStack {
                    Spacer()
                    Button(action: {
                        self.showAddView.toggle()
                    }) {
                        Image(systemName: "plus.circle.fill").resizable()
                            .frame(width: 40, height: 40)
                    }
                    .sheet(isPresented: $showAddView) {
                        AddView()
                    }
                    Spacer()
                    Button(action: {
                        self.showRemoveView.toggle()
                    }) {
                        Image(systemName: "minus.circle.fill").resizable()
                            .frame(width: 40, height: 40)
                    }
                    .disabled(globals.totalWater == 0)
                    .sheet(isPresented: $showRemoveView) {
                        RemoveView()
                    }
                    Spacer()
                }
            }
        }
        .preferredColorScheme(globals.darkMode ? .dark : .light)
        .padding()
        .onAppear {
            withAnimation(Animation.linear(duration: 2)
                .repeatForever(autoreverses: false)) {
                    self.waveOffset = Angle(degrees: 360)
                }
        }
    }
}

struct AddView: View {
    @EnvironmentObject var globals: GlobalVars
    @Environment(\.dismiss) private var dismiss
    var body: some View {
        VStack {
            Text("Add \(globals.changeWater+1) \(globals.units)")
            Picker("Add", selection: $globals.changeWater) {
                ForEach(1..<100) { index in
                    Text("\(index) \(globals.units)")
                }
            }
            HStack {
                Spacer()
                Button("Cancel") {
                    globals.changeWater = 0
                    dismiss()
                }
                Spacer()
                Button("Add") {
                    globals.totalWater += Double(globals.changeWater) + 1
                    globals.changeWater = 0
                    dismiss()
                }
                Spacer()
            }
        }
        .pickerStyle(.inline)
        .padding()
        .background(Rectangle()
            .foregroundColor(globals.darkMode ? .black : .white)
            .cornerRadius(50)
            .shadow(radius: 50))
        .padding()
        .onChange(of: globals.totalWater) { newValue in
            userDefaults.set(newValue, forKey: KeyValue.totalWater.rawValue)
            globals.percent = Double(globals.totalWater) / Double(globals.goal)
            userDefaults.set(globals.percent, forKey: KeyValue.percent.rawValue)
        }
    }
}

struct RemoveView: View {
    @EnvironmentObject var globals: GlobalVars
    @Environment(\.dismiss) private var dismiss
    var body: some View {
        VStack {
            Text("Remove \(globals.changeWater+1) \(globals.units)")
            Picker("Add", selection: $globals.changeWater) {
                ForEach(1..<(Int(globals.totalWater)+1)) { index in
                    Text("\(index) \(globals.units)")
                }
            }
            HStack {
                Spacer()
                Button("Cancel") {
                    globals.changeWater = 0
                    dismiss()
                }
                Spacer()
                Button("Remove") {
                    globals.totalWater -= Double(globals.changeWater + 1)
                    globals.changeWater = 0
                    dismiss()
                }
                Spacer()
            }
        }
        .pickerStyle(.inline)
        .padding()
        .background(Rectangle()
            .foregroundColor(globals.darkMode ? .black : .white)
            .cornerRadius(50)
            .shadow(radius: 50))
        .padding()
        .onChange(of: globals.totalWater) { newValue in
            userDefaults.set(newValue, forKey: KeyValue.totalWater.rawValue)
            globals.percent = Double(globals.totalWater) / Double(globals.goal)
            userDefaults.set(globals.percent, forKey: KeyValue.percent.rawValue)
        }
    }
}
        
struct SettingsView: View {
    @EnvironmentObject var globals: GlobalVars
    
    var body: some View {
        NavigationStack {
            List {
                Picker("Water Goal", selection: $globals.goal) { // water goal
                    ForEach(0..<100) { index in
                        Text("\(index) \(globals.units)")
                    }
                }
                .onChange(of: globals.goal) { newValue in
                    userDefaults.set(newValue, forKey: KeyValue.goal.rawValue)
                    globals.percent = Double(globals.totalWater) / Double(globals.goal)
                    userDefaults.set(globals.percent, forKey: KeyValue.percent.rawValue)
                }
                
                Picker("Select Units", selection: $globals.units, content: { // units selection
                    Text("cup (c.)").tag("c.")
                    Text("ounce (oz.)").tag("oz.")
                })
                .onChange(of: globals.units) { newValue in
                    userDefaults.set(newValue, forKey: KeyValue.units.rawValue)
                    if (globals.units == "oz.") {
                        globals.goal *= 8
                        globals.totalWater *= 8
                    } else {
                        globals.goal /= 8
                        globals.totalWater /= 8
                    }
                }
                
                Toggle("Dark Mode", isOn: $globals.darkMode)
                    .onChange(of: globals.darkMode) { newValue in
                        userDefaults.set(newValue, forKey: KeyValue.darkMode.rawValue)
                    }
                
                Toggle("Receive Notifications", isOn: $globals.notiOn) // toggle notification
                    .onChange(of: globals.notiOn) { newValue in
                        userDefaults.set(newValue, forKey: KeyValue.notiOn.rawValue)
                        if (newValue) {
                            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { success, error in
                                if (!success) {
                                    globals.notiOn = false
                                }
                            }
                        }
                    }
                
                NavigationLink ( // notification frequency
                    destination: SetNotificationView(),
                    label: {
                        HStack {
                            Text("Notification Frequency")
                            Spacer()
                            Divider()
                            Text("\(globals.notiHr) hr \(globals.notiMin) min")
                                .foregroundColor(.gray)
                        }
                    }
                )
                .disabled(!globals.notiOn)
                
                NavigationLink (
                    destination: TestFeaturesView(),
                    label: {
                        Text("Test Features")
                    }
                )
                
                NavigationLink (
                    destination: SourcesView(),
                    label: {
                        Text("Sources and Credits")
                    }
                )
            }
            .navigationTitle("Settings")
            .preferredColorScheme(globals.darkMode ? .dark : .light)
        }
    }
}

struct SetNotificationView: View {
    @EnvironmentObject var globals: GlobalVars
    @Environment(\.dismiss) private var dismiss
    var body: some View {
        VStack {
            Spacer()
            Text("Recieve Notifications Every \(globals.notiHr) hr \(globals.notiMin) min")
            VStack (spacing: 20){
                HStack {
                    VStack {
                        Text("Hour")
                        Picker("Select Notification Frequency", selection: $globals.notiHr){
                            ForEach(0..<24) { index in
                                Text("\(index)")
                            }
                        }
                        .onChange(of: globals.notiHr) { newValue in
                            userDefaults.set(newValue, forKey: KeyValue.notiHr.rawValue)
                        }
                    }
                    VStack {
                        Text("Minutes")
                        Picker("Select Notification Frequency", selection: $globals.notiMin){
                            ForEach(0..<60) { index in
                                Text("\(index)")
                            }
                        }
                        .onChange(of: globals.notiMin) { newValue in
                            userDefaults.set(newValue, forKey: KeyValue.notiMin.rawValue)
                        }
                    }
                }
            }
            .pickerStyle(.inline)
            .padding()
            .background(Rectangle()
                .foregroundColor(globals.darkMode ? .black : .white)
                .cornerRadius(50)
                .shadow(radius: 50))
            .padding()
            .navigationBarBackButtonHidden(true)
            .toolbar {
                ToolbarItem(placement: .navigationBarLeading) {
                    Button("Cancel") {
                        globals.notiHr = globals.prevNotiHr
                        globals.notiMin = globals.prevNotiMin
                        dismiss()
                    }
                }
                ToolbarItem(placement: .principal) {
                    Text("Set Notification Frequency")
                        .font(.headline)
                }
                ToolbarItem(placement: .navigationBarTrailing) {
                    Button("Save") {
                        globals.prevNotiHr = globals.notiHr
                        globals.prevNotiMin = globals.notiMin
                        dismiss()
                    }
                }
            }
            .padding(.bottom, 280)
        }
    }
}

struct TestFeaturesView: View {
    @EnvironmentObject var globals: GlobalVars
    @Environment(\.dismiss) private var dismiss
    @State private var displayNotiText = false
    var body: some View {
        VStack {
            List {
                VStack {
                    HStack {
                        Text("Send Notification")
                        Spacer()
                        Button(action: {
                            if (globals.notiOn) {
                                let content = UNMutableNotificationContent()
                                content.title = "Time to Rehydrate!"
                                content.body = "You haven't drank water in the past hour"
                                content.sound = UNNotificationSound.default
                                
                                let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 4, repeats: false)
                                let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: trigger)
                                UNUserNotificationCenter.current().add(request)
                                displayNotiText = true
                                Timer.scheduledTimer(withTimeInterval: 2, repeats: false) { _ in
                                    displayNotiText = false
                                }
                            }
                        }) {
                            Image(systemName: "iphone.gen2.circle").resizable()
                                .frame(width: 40, height: 40)
                        }
                    }
                    HStack {
                        if (displayNotiText) {
                            Text("Sent.")
                        }
                        Spacer()
                    }
                }
                HStack {
                    Text("Reset to Default Settings")
                    Spacer()
                    Button(action: {
                        
                    }) {
                        Image(systemName: "arrow.triangle.2.circlepath").resizable()
                            .frame(width: 40, height: 35)
                    }
                    .frame(width: 40, height: 40)
                }
            }
        }
    }
}

struct SourcesView: View {
    var body: some View {
        VStack {
            List {
                Group {
                    Text("YouTube Channels:")
                    Link("CodeWithChris", destination: URL(string: "https://www.youtube.com/@CodeWithChris")!)
                    Link("DevTechie", destination: URL(string: "https://www.youtube.com/@DevTechie")!)
                    Link("Paul Hudson", destination: URL(string: "https://www.youtube.com/@twostraws")!)
                    Link("Indently", destination: URL(string: "https://www.youtube.com/@Indently")!)
                }
                Group {
                    Text("Websites:")
                    Link("Sarunw", destination: URL(string: "https://www.sarunw.com")!)
                    Link("Hacking With Swift", destination: URL(string: "https://www.hackingwithswift.com")!)
                    Link("Simple Swift Guide", destination: URL(string: "https://www.simpleswiftguide.com")!)
                    Link("The Swift Programming Language", destination: URL(string: "https://docs.swift.org/swift-book/documentation/the-swift-programming-language")!)
                    Link("Stack Overflow", destination: URL(string: "https://www.stackoverflow.com")!)
                    Link("Apple Developer", destination: URL(string: "https://developer.apple.com")!)
                    Link("UserDefaults Tutorial", destination: URL(string: "https://medium.com/geekculture/swiftui-tutorial-persistent-data-using-userdefaults-and-appstorage-c17a94e131cf")!)
                    Link("SF Symbols", destination: URL(string: "https://developer.apple.com/sf-symbols/")!)
                }
            }
            .navigationTitle("Resources")
        }
    }
}

// This is not my original solution. Solution credits to DevTechie: https://www.youtube.com/@DevTechie
struct Wave: Shape {
    var offset: Angle
    var percent: Double
    var animatableData: Double {
        get { offset.degrees }
        set { offset = Angle(degrees: newValue) }
    }
    
    func path(in rect: CGRect) -> Path {
        var path = Path()
        let waveHeight = 0.012 * rect.height
        let yOffset = CGFloat(1 - percent) * (rect.height - 2 * waveHeight) + 1 * waveHeight
        let startAngle = offset
        let endAngle = offset + Angle(degrees: 360)
        
        path.move(to: CGPoint(x: 0, y: yOffset + waveHeight * CGFloat(cos(offset.radians))))
        for angle in stride(from: startAngle.degrees, through: endAngle.degrees, by: 1) {
            let x = CGFloat((angle - startAngle.degrees) / 360) * rect.width
            path.addLine(to: CGPoint(x: x, y: yOffset + waveHeight * CGFloat(cos(Angle(degrees: angle).radians))))
            
        }
        path.addLine(to: CGPoint(x: rect.width, y: rect.height))
        path.addLine(to: CGPoint(x: 0, y: rect.height))
        path.closeSubpath()
        return path
    }
}