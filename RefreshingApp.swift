//
//  RefreshingApp.swift
//  Refreshing
//
//  Created by William Ong on 3/24/23.
//

import SwiftUI

@main
struct RefreshingApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
